<!-- --------------------------------------------
-- title : Robot API
-- author : 
-- description : Fonctions et syntaxe pour la programmation des robots
-- source : In-Game Help from basic_robot mod
--  converted to markdown syntax but still importable into a formspec
-- (...still waiting for in-game markdown parser)				
------------------------------------------------- -->

# Robot API
Ce document présente la plupart des fonctions et la syntaxe  pour la programmation des robots.

### BASIC LUA SYNTAX

la plupart des syntaxes basique du langage lua sont autorisés.
>	if x==1 then A else B end
>	for i = 1, 5 do something end
>	while i<6 do A; i=i+1; end

Tableaux:

>	myTable1 = {1,2,3},  myTable2 = {[\"entry1\"]=5, [\"entry2\"]=1}

( Voir la documentation sur le langage lua pour plus de détails )

## COMMANDES POUR LES ROBOTS

### MOVEMENT,DIGGING, PLACING, INVENTORY TAKE/INSERT

`move.direction()`
`turn.left()`, `turn.right()`, `turn.angle(45)`
`dig.direction()`
`place.direction('default:dirt', optional orientation param)`
`read_node.direction()`
`insert.direction(item, inventory)`
`check_inventory.direction(itemname, inventory, index)`
`activate.direction(mode)`
`pickup(r)`
`craft(item,mode)`
`take.direction(item, inventory)`
`read_text.direction(stringname,mode)`
`write_text.direction(text,mode)` 

---------------------------------

`move.direction()`
Où direction peut être : forward, backward, left,right, up, down
La direction forward_down ne fonctionne qu'avec `dig`, `place` et `read_node`

`turn.left()`, `turn.right()`, `turn.angle(45)`

`dig.direction()`

`place.direction('default:dirt', optional orientation param)`

`read_node.direction()`
Renvoie le nom du bloc

`insert.direction(item, inventory)` 
Ajoute un objet dans l'inventaire cible depuis l'invetaire du robot

`check_inventory.direction(itemname, inventory, index)`
Inspecte le bloc et renvoie false/true
- direction peut être self
- si index > 0 , renvoie itemname. Si itemname == '' , test si l'inventaire est vide.

`activate.direction(mode)`
Active le bloc cible

`pickup(r)`
Rammasse tous les objets autours du robot dans un rayon de r. r doit être < 8. Renvoie la liste des objets ou nil.

`craft(item,mode)`
Craft item si les matériaux nécessaires sont présents dans l'inventaire.
mode = 1  renvoie la recette

`take.direction(item, inventory)`
Prends item dans l'inventaire cible et l'ajoute à celui du robot.

`read_text.direction(stringname,mode)`
Lit le texte d'un panneau, coffre, ou autre bloc.
Paramètre stringname optionnel pour d'autres meta, mode 1 s'il s'agit d'un nombre

`write_text.direction(text,mode)` 
Écrit le texte dans le bloc cible (en tant qu'infotext)

### BOOKS/CODE

`title,text=book.read(i)` 
`book.write(i,title,text)`
`code.run(text)`
`code.set(text)`
`find_nodes('default:dirt',3)`

---------------------------------

`title,text=book.read(i)` 
Renvoie titre, contenut du livre à la position i de la bibliothèque

`book.write(i,title,text)`
Ecrit dans le livre à la position i de la bibliothèque

`code.run(text)`
compile et execute le code dans la sandbox

`code.set(text)`
remplace le code actuel du robot

`find_nodes('default:dirt',3)`
renvoie la distance du bloc dans un rayon de 3 ou false s'il n'y en a pas

### PLAYERS

`find_player(3)`
`attack(target)`
`grab(target)`
`player.getpos(name)`

`find_player(3)`
finds players in radius 3 around robot and returns list, if none returns nil

`attack(target)`
attempts to attack target player if nearby

`grab(target)`
attempt to grab target player if nearby and returns true if succesful

`player.getpos(name)`
return position of player, player.connected() returns list of players

### ROBOT

`say(\"hello\")`
`self.listen(0/1)`
`speaker, msg = self.listen_msg()`
`self.send_mail(target,mail)`
`sender,mail = self.read_mail()`
`self.pos()`
`self.name()`
`self.set_properties({textures=.., visual=..,visual_size=.., , )`
`self.set_animation(anim_start,anim_end,anim_speed,anim_stand_start)`
`self.spam(0/1)`
`self.remove()`
`self.reset()`
`self.spawnpos()`
`self.viewdir()`
`self.fire(speed, pitch,gravity)`
`self.fire_pos()`
`self.label(text)`
`self.display_text(text,linesize,size)`
`self.sound(sample,volume)`

---------------------------------


`say(\"hello\")`
will speak

`self.listen(0/1)`
(de)attaches chat listener to robot

`speaker, msg = self.listen_msg()`
retrieves last chat message if robot listens

`self.send_mail(target,mail)`
sends mail to target robot
`sender,mail = self.read_mail()`
reads mail, if any
`self.pos()`
returns table {x=pos.x,y=pos.y,z=pos.z}
`self.name()`
returns robot name
`self.set_properties({textures=.., visual=..,visual_size=.., , )`
sets visual appearance
`set_animation(anim_start,anim_end,anim_speed,anim_stand_start)`
set mesh animation
`self.spam(0/1)`
(dis)enable message repeat to all
`self.remove()`
stops program and removes robot object
`self.reset()`
resets robot position
`self.spawnpos()`
returns position of spawner block
`self.viewdir()`
returns vector of view for robot
`self.fire(speed, pitch,gravity)`
fires a projectile from robot
`self.fire_pos()`
returns last hit position
`self.label(text)`
changes robot label
`self.display_text(text,linesize,size)`
displays text instead of robot face, if no size return text
`self.sound(sample,volume)`
plays sound named 'sample' at robot location
rom is aditional table that can store persistent data, like `rom.x=1`
.
### KEYBOARD
place spawner at coordinates (20i,40j+1,20k) to monitor events

`keyboard.get()`
`keyboard.set(pos,type)`
`keyboard.read(pos)`

---------------------------------

`keyboard.get()`
returns table {x=..,y=..,z=..,puncher = .. , type = .. } for keyboard event
`keyboard.set(pos,type)`
set key at pos of type 0=air, 1..6, limited to range 10 around
`keyboard.read(pos)`
return node name at pos

### TECHNIC FUNCTIONALITY
namespace 'machine'. most functions return true or nil, error

`machine.energy()`
`machine.generate_power(fuel, amount)`
`machine.smelt(input,amount)`
`machine.grind(input)`
`machine.compress(input)`
`machine.transfer_power(amount,target_robot_name)`

---------------------------------

`machine.energy()`
displays available energy
`machine.generate_power(fuel, amount)`
= energy, attempt to generate power from fuel material
	if amount>0 try generate amount of power using builtin generator - this requires 40 gold/mese/diamonblock upgrades for each 1 amount
`machine.smelt(input,amount)`
= progress/true. works as a furnace
	if amount>0 try to use power to smelt - requires 10 upgrades for each 1 amount, energy cost is 1/40*(1+amount)
`machine.grind(input)`
 grinds input material, requires upgrades for harder material
`machine.compress(input)`
requires upgrades - energy intensive process
`machine.transfer_power(amount,target_robot_name)`

### CRYPTOGRAPHY
namespace 'crypto'

`crypto.encrypt(input,password)`
`crypto.decrypt(input,password)`
`crypto.scramble(input,randomseed,sgn)`
`crypto.basic_hash(input,n)`

---------------------------------

`crypto.encrypt(input,password)`
returns encrypted text, password is any string
`crypto.decrypt(input,password)`
attempts to decrypt encrypted text
`crypto.scramble(input,randomseed,sgn)`
 (de)permutes text randomly according to sgn = -1,1
`crypto.basic_hash(input,n)`
returns simple mod hash from string input within range 0...n-1
