-- This mod add a way to import programs for basic_robot
-- 
-- TODO : Allow importing file from clients instead or in addition of worldpath

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local worldpath = minetest.get_worldpath()

-- Load support for intllib.
local S, NS = dofile(modpath.."/intllib.lua")
--	S = function(s) return s end

-- Request write access into the world folder
local ie = minetest.request_insecure_environment()
assert(ie, "You must allow `basic_robot_terminal` in `secure.trusted_mods`")

local private = { }
private.open = ie.io.open
private.mkdir = ie.core.mkdir

local brt = {}
brt.loglevel = "info"
brt.main_categories = {
	-- Denomination, Prefix, Description
	{S("Documentation"), "robot_doc", S("Documentation about the robot usage.")},
	{S("Scripts"), "scripts_index", S("A library of scripts for the robot")},
}
brt.scripts_categories = {
	-- Denomination, Prefix, Description
	{S("Exemples"), "exemple", S("A set of exemples scripts for the robot")},
	{S("Exemples (advanced)"), "exemple_advanced", S("A set of advanced exemples scripts for the robot")},
	{S("Testing"), "testing", S("Scripts that may not work and need testing")},
	{S("Admin"), "admin", S("Scripts that are requiering some admin privilege to run")},
	}
-- Filestructure into worldpath
brt.dir = worldpath.."/basic_robot"

-- Pattern identifiyng files infos
brt.patterns = {}
brt.patterns.lua = {
	{ "title", "-- title : " },
	{ "author", "-- author : " },
	{ "description", "-- description : "},
}

---------------
-- Functions --
---------------

-- Send message to minetest log
brt.log = function(text,level)
	if not level then level = brt.loglevel end
	minetest.log(level,"["..modname.."] "..text)
end

-- Send notification message to player
brt.notify = function(player, text)
	local name = player:get_player_name()
	minetest.sound_play({ name = modname.."_blop", gain = 0.2 }, { to_player = name })
	minetest.chat_send_player(name,text)
end

-- Test if player is admin (has the privs priviliege)
brt.player_is_admin = function(player)
	local name = player:get_player_name()
	local privs = minetest.get_player_privs(name)
	if privs and privs.privs then return true
	else return false
	end
end

--______________________________________________________________
--
-- Part 1 : Import scripts from modpath into worlpath
--______________________________________________________________

-- Function : Copy content of a file into an other file
brt.copy = function(inpath,outpath)
	
	local input = private.open(inpath,"r")
	local output = private.open(outpath,"w")
	-- If file successfully opened ... 
	if input and output then
		-- write file content to the one in worldpath
		local content = input:read("*all")
		output:write(content)
		output:close()
		input:close()
	else
		brt.log("Uable to copy content of "..inpath.." to "..outpath)
	end
end

local scpath = modpath.."/scripts"

-- Create the basic_robot directory into the world folder
private.mkdir(brt.dir)

-- Copy scripts files gathered in this mod
-- Including scripts by rnd contained in basic_robot
for _,v in ipairs(brt.scripts_categories) do
	local n = 1
	while true do
		local sc = scpath.."/"..v[2].."_"..n..".lua"
		local fd = private.open(sc)
		if not fd then break end
		fd:close()
		local dst = brt.dir.."/"..v[2].."_"..n..".lua"
		brt.copy(sc,dst)
		n = n + 1
	end
end



--_____________________________________________________________________
--
-- Part 2 : Use a chatcommand to import a single script into inventory
--_____________________________________________________________________

--[[
minetest.register_chatcommand("terminal", {
	privs = {
--		server = true
	},
	params = S("<name>"),
	description = S("Display the starter kit choice message"),
	func = function(name, param)
		if not param or param == "" then
			param = name
		end
		local player = minetest.get_player_by_name(param)
		brt.terminal(player)
	end
})
--]]

--______________________________________________________________
--
-- Part 2 : Save a book to a file
--______________________________________________________________

brt.save_to_book =  function(player,text,confirm)
	local msg
	
	-- Check that there is something to save
	if not text then
		msg = S("Sorry, no content to save...")
		brt.notify(player,msg
		return
	end

	-- Check that there is a book to save to
	local inv = player:get_inventory()
	local has_empty_book = inv:contains_item("main", "default:book")
	if not has_empty_book then
		msg = S("You need at least one empty book in your inventory")
		brt.notify(player,msg
		return
	end
	
	-- Ask confirmation
	if comfirm then
		msg = S("Copy script into an empty book from your inventory ?")
		local formspec = "size[7,3,true]" .. deco
			.."textarea[0.4,0;6.4,2;;"..minetest.formspec_escape(msg)..";]"
			.. "button_exit[1.5,2;1.5,1;confirm;"..S("Yes").."]"
			.. "button_exit[4.5,2;1.5,1;abort;"..S("No").."]"
			-- hidden area containing the text to copy
			.."field[-1,-1;0,0;previous;;"..fields.script.."]"
		local formname = modname..":confirm_save_to_book"
		
		brt.showformspec(player,formspec,formname)
		return
	
	else -- Make the copy
	local written_book = 
	inv:remove_item("main", "default:book")	
	if inv:room_for_item ("main", "default:book_written") then
		add_item(listname, stack)	

end
--______________________________________________________________
--
-- Part 2 : Access scripts and doc using a formspec
--______________________________________________________________
brt.lists = {}
brt.lists.scripts = {}
brt.lists.docs = {}

local get_file_infos = function(rep, prefix, ext)
	local lst = {}
	local n = 1
	while true do
		local sc = rep.."/"..prefix.."_"..n.."."..ext
		local f = private.open(sc)
		if not f then break end
		lst[n] = {}
		-- This whole thing is pretty heavy...
		-- Can't I just get line 1,2,3 ?
		local l = 1
		for line in f:lines() do
			local patterns
			-- Look fot set of patterns specific to this extention
			if brt.patterns[ext] then patterns = brt.patterns[ext]
			-- Or default to lua
			else patterns = brt.patterns.lua end
			
			-- Look for pattern indicating infos
			for _,v in ipairs(patterns) do
				if string.match(line,v[2]) then 
					local k = v[1]
					lst[n][k] = string.gsub(line,v[2],"")
					break
				end	
			end
			-- Break the loop after 4 lines
			l = l + 1
			if l > 4 then break end
		end
		f:close()
		if not lst[n]["title"] then lst[n]["title"] = listname.."_"..n end
		-- if not lst[n]["author"] then lst[n]["author"] = "" end
		lst[n]["path"] = sc
		n = n + 1
	end
	return lst
end

brt.lists_update = function()
	-- Reinitialize the list of scripts
	brt.lists.scripts = {}

	-- Update list of scripts to match content of the worldpath rep
	for _,v in ipairs(brt.scripts_categories) do
		local listname = v[2]
		local lst = get_file_infos(brt.dir,listname,"lua")
		brt.lists.scripts[listname] = lst
	end
end

brt.lists_make = function()
	brt.lists_update() 	
	local docpath = modpath.."/docs"
	brt.lists.docs = get_file_infos(docpath,"robot_doc","md")
end

local line_parse = function(line)
	local op = ","
	local pt = {
		{"^# ","#FF9292,"},
		{"^## ","#9292DB,"},
		{"^### ","#92FF92,"},
		{"^#### ","#FFFF00,"},
	}
	for _,v in ipairs(pt) do
		if string.match(line,v[1]) then
			line = string.gsub(line,v[1],"")
			op = v[2]
			break
		end
	end
	line = op .. minetest.formspec_escape(line)
	return line
end

brt.form = {}

-- Function : main form content displaying text
brt.form.text = function(ref)
	local ta, filetext, filettable
	-- Read file to get its content
	local f = private.open(ref.path)
	if not f then filetext = S("Sorry, there was a problem while trying to read the file...") end
	-- If we just want to read the file
	if ref.read then
		-- Lets import it as a table
		filettable = {}
		maxlen = 59
		for line in f:lines() do
			-- Test if string is too long
			while true do 
				local len = string.len(line)
				if len > maxlen then
					-- Get a resized substring
					local sub = string.sub(line,1,maxlen)
					-- Match the longest sequence finishing with a space in this substring
					local match = string.match(sub, "^(.*%s)")
					-- If no match, ... split anyways
					if not match then match = sub end
					-- Get length of this new substring
					local matchlen = string.len(match)
					-- Add it to table
					filettable[#filettable+1] = line_parse(match)
					-- Refefine line
					line = string.sub(line, matchlen + 1, len )
				else
					-- TODO Remove comments at the beginning (keep them as tooltip)
					-- even prepare it for formspec
					filettable[#filettable+1] = line_parse(line)
					break
				end
			end
		end
		print(dump(filettable))
	-- Otherwise we need be able to select and edit and copy and paste, so string it is
	else filetext = f:read("*all") end
	f:close() 

	-- Display the text
	-- Docs as readable text
	if ref.read then
		 -- Table settings	
			ta = "tablecolumns[color;text,align=inline]" --,width=5 
			.. "tableoptions[color=#FFF;background=#494949;highlight=#494949;border=true]" 
			.. "table[0,0;6.7,6;doc;" .. table.concat(filettable, ",") .. ",] "

	-- Script text into a textarea
	else
		ta = "textarea[0.4,0;6.7,6.7;script;;"..minetest.formspec_escape(filetext).."]"
	end
	-- Add a button to get back to the previous menu
	ta = ta .. "image_button[1,6.5;0.8,0.8;parent.png;parent;;false;true;parent.png]"
	-- Hidden field containing name of previous menu
	ta = ta .. "field[-1,-1;0,0;previous;;"..ref.previous.."]"
	
	-- TODO : Save button for later implementation of a save to a book option
	-- ta = ta .. "image_button[2,6.5;0.8,0.8;save.png;save;;false;true;save.png]"

	return ta
end

-- Function : main form content displaying a menu 
brt.form.menu = function(player,ref,i)
	local description
	if ref == "main_index" then bck = "." 
	else bck = ".." end

	-- Start the textlist item
	local menu = "textlist[0,0;6.7,5.5;"..ref..";"..bck
	local first = true

	-- Main menu
	if ref == "main_index" then
		-- Loop through categories
		for _,v in ipairs(brt.main_categories) do
			-- Add button	
			local label = v[1]	
			menu = menu .. "," .. minetest.formspec_escape(label)
		end
		-- Get description
		if i and brt.main_categories[i] then description = brt.main_categories[i][3] end

	-- Documentation
	elseif ref == "robot_doc" then
		-- Loop through table containing scripts in categories
		for _,v in ipairs(brt.lists.docs) do
			local credit = ""
			-- Add button	
			if v.author and v.author ~= "" then credit = " (by "..v.author..")" end
			local label = v.title..credit	
			menu = menu .. "," .. minetest.formspec_escape(label)
		end
		if i and brt.lists.docs[i] then description = brt.lists.docs[i]["description"] end
		
	-- Scripts menu
	elseif ref == "scripts_index" then
		local has_privs = brt.player_is_admin(player)
		-- Loop through categories
		for _,v in ipairs(brt.scripts_categories) do
			
			-- Skip admin category if player is not admin
			if  ( v[2] == "admin" and not has_privs ) then 
			else
				-- Add button	
				local label = v[1]	
				menu = menu .. "," .. minetest.formspec_escape(label)
			end
		end
		-- Get description
		if i and brt.scripts_categories[i] then description = brt.scripts_categories[i][3] end
	
	-- Any scripts submenu
	else
		-- Loop through table containing scripts in categories
		for _,v in ipairs(brt.lists.scripts[ref]) do
			local credit = ""
			-- Add button	
			if v.author and v.author ~= "" then credit = " (by "..v.author..")" end
			local label = v.title..credit	
			menu = menu .. "," .. minetest.formspec_escape(label)
		end
		if i and brt.lists.scripts[ref][i] then description = brt.lists.scripts[ref][i]["description"] end
	end


	-- Close the textlist item
	if i then menu = menu .. ';]' 
	else menu = menu .. ';1]' end 	
	
	-- Display description
	if description then 
		menu = menu .. "textarea[0.5,5.7;5,2;; ".. minetest.formspec_escape(description) ..";]" 	
	end
	return menu
end

-- Function display the formpec to the player
brt.showformspec = function(player,formspec,formname)
   if not formname then formname = modname..":terminal" end
   local player_name = player:get_player_name();
   if not player_name or player_name == "" then return; end
   
   minetest.show_formspec(player_name,formname,formspec)
end

local deco = default.gui_bg .. default.gui_bg_img
local formspec_base = "size[7,7,true]" .. deco
local formspec_content
local formspec_end = "image_button_exit[6,6.5;0.8,0.8;power.png;poweroff;;false;false;power.png]"	
-- tooltip[<gui_element_name>;<tooltip_text>;<bgcolor>;<fontcolor>]
brt.terminal = function(player,ref,i)
	-- Set index to default
	if not ref then ref = "main_index" end
	--	if not i then i = 1 end

	-- Make or update files lists
	if #brt.lists.docs < 1 then brt.lists_make()
	else brt.lists_update() end
	
	
	if type(ref) == "table" then formspec_content = brt.form.text(ref)
	else formspec_content = brt.form.menu(player,ref,i)
	end
	local formspec = formspec_base .. formspec_content .. formspec_end
	brt.showformspec(player, formspec)
end

--______________________________________________________________
--
-- Part 3 : Interpret data from fromspec
--______________________________________________________________
	
minetest.register_on_player_receive_fields(function(player, formname, fields)
	
	if formname == modname..":terminal" then
		print(dump(fields))
		local name = player:get_player_name()

		-- Get back to previous menu from file view
		if fields.parent and fields.previous then 
			brt.terminal(player,fields.previous)

		-- TODO save text into an empty book
		elseif fields.save_to_book then 
			--local msg = S("Sorry, the 'save to book' functionnality hasn't been implemented yet...")
			--			.."\n"..S("... just use the old CRTL+C, CTRL+V, for now")
			--brt.notify(player,msg)
			brt.save_to_book(player,fields)

		-- Fields came from main menu 
		elseif fields.main_index then
			-- Get event data
			local event = minetest.explode_textlist_event(fields.main_index)
			local i = event.index - 1
			
			-- Double Click ( ignore first entry )
			if event.type == "DCL" and i ~= 0 then
				-- move to selected menu
				brt.terminal(player,brt.main_categories[i][2])
			-- Simple Click : Update description
			else
				brt.terminal(player,"main_index",i) 
			end
		
		-- fields came from docs index
		elseif fields.robot_doc then
			-- Get event data
			local event = minetest.explode_textlist_event(fields.robot_doc)
			local i = event.index - 1

			-- Double Click 
			if event.type == "DCL" then
				-- First entry : Back to main menu
				if i == 0 then brt.terminal(player,"main_index")
				else
					-- Display selected file
					local tab = brt.lists.docs[i]
					tab.previous = "robot_doc"
					tab.read = true
					brt.terminal(player,tab)
				end
			-- Simple Click : Update description
			else
					brt.terminal(player,"robot_doc",i) 
			end
				
		-- fields came from scripts index
		elseif fields.scripts_index then
			-- Get event data
			local event = minetest.explode_textlist_event(fields.scripts_index)
			local i = event.index - 1
			
			-- Double Click : 
			if event.type == "DCL" then
				-- First entry : Back to main menu
				if i == 0 then brt.terminal(player,"main_index")
				-- Otherwise, move to selected categorie
				else brt.terminal(player,brt.scripts_categories[i][2])
				end 			
			-- Simple Click : Update description
			else
				brt.terminal(player,"scripts_index",i) 
			end
		-- Fields were received from inside a categorie
		else
			-- Loop through existing categories to find the right one
			for _,v in ipairs(brt.scripts_categories) do
				-- Get categorie name
				local c = v[2] 
				if fields[c] then
					-- Get event data
					local event = minetest.explode_textlist_event(fields[c])
					local i = event.index - 1

					-- Double Click 
					if event.type == "DCL" then
						-- First entry : Back to main menu
						if i == 0 then brt.terminal(player,"scripts_index")
						else
							-- Display selected file
							local tab = brt.lists.scripts[c][i]
							tab.previous = v[2]
							brt.terminal(player,tab)
							--brt.terminal(player,brt.scripts_categories[i][2],i) 			
						end
					-- Simple Click : Update description
					elseif i ~= 0 then
						brt.terminal(player,c,i) 
					end
				end
			end	
		end
	end

end)

--______________________________________________________________
--
-- Part 4 : Register a chat command tool
--______________________________________________________________

-- TODO : chat command to list available scripts and to display one, or copy to a book
--[[
-- Chat command : display the formspec
minetest.register_chatcommand("terminal", {
	privs = {
--		server = true
	},
	params = S("<name>"),
	description = S("Display the starter kit choice message"),
	func = function(name, param)
		if not param or param == "" then
			param = name
		end
		local player = minetest.get_player_by_name(param)
		brt.terminal(player)
	end
})
--]]
--______________________________________________________________
--
-- Part 5 : Use a block to display fromspec
--______________________________________________________________

	
local texture = {
	front = modname.."_front.png",
	back = modname.."_side.png",
	top = modname.."_side.png",
	bottom = modname.."_side.png",
	left = modname.."_side.png",
	right = modname.."_side.png",
}

minetest.register_node(modname..":terminal",
   {
      description = S("Basic robot terminal"),
      paramtype2 = "facedir",
	  tiles = {
			texture.top, texture.bottom,
			texture.left, texture.right,
			texture.back, texture.front
		},
      sounds = default.node_sound_wood_defaults(),
      groups = { choppy = 3, oddly_breakable_by_hand = 2 },
	  on_rightclick = function(pos, node, player, itemstack, pointedThing)
        brt.terminal(player);
      end
   });

-- Register Recipe for the terminal block
-- ( if default mod is present )
if minetest.get_modpath("default") then

	-- Default recipe
	local r = {}
	r.c = "default:tin_ingot"
	r.m = "default:bookshelf"
	r.t = "default:mese_crystal_fragment"
	r.d = "default:mese_crystal_fragment"
	r.l = ""
	r.r = ""

	-- Changes if mesecons
	if minetest.get_modpath("mesecons") then
		r.d = "mesecons:wire_00000000_off"
		r.t = "mesecons:wire_00000000_off"
		r.l = "mesecons:wire_00000000_off"
		r.r = "mesecons:wire_00000000_off"
	end

	--[[ other changes if dye
	if minetest.get_modpath("dye") then
		r.l = "mesecons:magenta"
		r.t = "mesecons:green"
		r.r = "mesecons:cyan"
	end
	--]]

	minetest.register_craft(
	   {
		  output = modname..":terminal",
		  recipe = { 
					{ r.c, r.t, r.c},
					{ r.l, r.m, r.r},
					{ r.c, r.d, r.c},
				}
	   });
	  
end
