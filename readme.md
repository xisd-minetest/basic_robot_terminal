# Basic Robot Terminal

*Documentation is not up to date* and need to be remade

This add a craftable terminal unit related to the [basic_robot](https://github.com/ac-minetest/basic_robot/) mod.
It can be used to consult documentation about robots and browse some scripts contained into the world folder

I made it to use in a subgame that is meant to be used in surival mode, without any admin privileges and without basic machines. 
Scripts and documentation included are focused on only basic robot

-[x] Fromspec window to browse scripts
-[x] Also provide some documentation
-[x] Terminal block to acces the formspec
-[ ] Documentation book (and treasurer support)
-[ ] Book storage into the terminal block
-[ ] Copy script to a book from the terminal 
-[ ] Save scripts to external file 

As a bonus : The documentation is stored in markdown format in the docs folder and is also converted into one single html file (robot_doc.html)




License: 
I tryed to credit scripts and other things I took a the beginings of files
Some texture files are from the basic_robot mod
If I forgot something tell me I will add it
and for the rest : code LGPL 2.1 media CC BY-SA 3.0
