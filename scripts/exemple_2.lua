-- title : Hello_World_2
-- author : 
-- description : Outputs "Hi !" (only once), then start counting and turning.
-- source : https://wiki.minetest.net/Mods/basic_robot
--

if (i == nil) then 
   say("Hi !")
   self.label("Hugo")
   i = 0 
end
 
i = i+1
turn.left()
say(i)


-- Outputs "Hi !" (only once), then start counting and turning.
--
--    == is the check to compare the values on both sides, i.e. "if i is-equal-to nil". 
--   = is an assignment, i.e. "i=0" means "put the value 0 into the variable i". 

-- To calculate i+1, the variable i must have a value assigned first.
-- And because all the code is executed again every second,
-- this first assignment must be done only once.
 
--   (Otherwise, we couldn't count to 2)

-- At the start, no variables exist yet, so we can test for nil ("no-value").
--    Note: nil is different from any string or number ! 
