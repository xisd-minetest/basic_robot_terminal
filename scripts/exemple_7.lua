-- title : Run_2
-- author : 
-- description : Essentially the same program as 'Run_1', but with more checking and reporting
-- source : https://wiki.minetest.net/Mods/basic_robot
--

 local function report_pos(msg,complement)
   p = self.pos()
   say(msg .. " position x=" .. p.x .. " y=" .. p.y .. " z=" .. p.z .. "/n" .. complement)
 end
 --
 if (i==nil) then say("\nDemo2"); i=0 end
 --
 n = read_node.forward()
 if n~="air" then
   report_pos("Now at") 
   say("found "..n..", turning")
   turn.right()
 else
   if move.forward() then 
     i=i+1  -- move was ok
   else
     report_pos("Cannot move at","(probably because I would need to fly to go there)")
     --self.remove()  -- stop program, and remove robot
   end
 end

if i > 20 then
	say("After 20 steps, I turn without any reason, this way I avoid getting stuck")
	if math.random(1,2) == 1 then turn.right() else turn.left() end 
	i=1
end

-- This is essentially the same program as 'Run1' above, but with more checking and reporting:
-- now there is a check if the move was successful, and output to report the position.
--
-- Because we want to report the current position of the bot at more then one place in the code,
-- the common instructions for that have been moved into a function.
-- When called, the function gets a string as a parameter, that is used in the message that is generated.
-- There is no return-statement at the end of the function, because we don't need a result.

--    ".." is the lua-command for concatenating strings.
--   "\n" is newline.
--    "~=" is the check for not-equal.
--    "--" starts a comment. Text after this until the end of the line is ignored. 

--    Here, we have also commented-out the statement "self.remove()", 
 --   so that it doesn't get executed. 

