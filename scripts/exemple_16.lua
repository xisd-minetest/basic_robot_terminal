-- title : Cobble Stair
-- author : ixfud
-- description :  Build cobble stairway from the bottom and dig if necessary. it needs cobble and tree to create energy, and optionnally torches.
-- source : 
--

-- .
-- TODO Change variable "tower_mode" to true to prevent digging while climbing
-- TODO Change variable "spiral_mode" to false to clime a straigth line prevent digging while climbing

local c = "default:cobble"
local t = "default:torch"
local d = "default:dirt"
local s = "stairs:stair_cobble"
local p = self.pos()
local r = 1

if not l then l = 1 
elseif l > 4 then 
    -- return
    l = 1 
end

local e = machine.energy() 
say(e)

local cmb = {"default:tree", "default:pine_tree", "default:jungle_tree", "default:aspen_tree", "default:acacia_tree", "default:coal_lump"}
if e < 1 then
	local src
    for _,s in ipairs(cmb) do
        if check_inventory.self(s) then src = s ; break ; end
    end
    if not src then
      say("I can't create energy ! Give me some wood or coal !")
       l = 404
    else
      say('Creating energy from '..src)
     machine.generate_power(src)
    end 
    return
end

if check_inventory.self(c) == false then 
    say("I need cobble !")
    return
end
if check_inventory.self(s) == false then 
    say("Ho I have to craft some stairs ...")
    if craft(s) then say("... done")
    else
       l = 404
      say("Oups, I can't do that ! I may be short of cobble.")
      return 
    end
end


if l == 1 then r = 1
elseif l == 2 then r = 24
elseif l == 3 then r = 3
elseif l == 4 then r = 4
end

if not a then a = "place_stair" end
if not i then i = 1 end

if a == "place_stair" then
	if not hold then 
     if i == 1 then say("Placing stairs ...")
	   elseif i == 2 then move.left()
   	elseif i == 3 then move.right() ; move.right()
   	else 
	   	move.left()
      a = "move_stair"
	   	i = 1
	    return
    end
  else
       hold = nil
  end
	n = read_node.forward()
	if n ~= "air" then
		dig.forward()
   hold = true
		return
	else
  	 place.forward(s,r)
     i = i + 1
   end
end		


if a == "move_stair" then
	if i == 1  then dir = "up"
	elseif i == 2 then dir = "forward"
	end
	
	n = read_node[dir]()
	if n ~= "air" then dig[dir]() ; return end
	move[dir]()
					
	i = i + 1
	if i > 2 then 
		a = "do_floor"
		i = 1
		return
	end
end

if a == "do_floor" then
	if not j then
      say("Let's make sure there is a floor")
      j = 1 
	end
	
	if j == 1 then dir = "forward_down"
	elseif j == 2 then 
		n = read_node.forward()
        if n ~= "air" then dig.forward() ; return
        else move.forward(); dir="left_down"
        end
	elseif j == 3 then dir = "right_down"
	end

	if i < 4 then 
		n = read_node[dir]()
		-- say('I should place '.. c.. ' '..dir)
		if n == "air" then 
			place[dir](c) 
			-- say("Therefore I place "..c)
		end
		j = j  + 1
		if j > 3 then
		  j = 1
		  i = i + 1
		end
		return
	elseif i > 6 then 
		n = read_node.up()
		if n ~= "air" then dig.up()
		else
			move.up()
			a = "clear_floor"
			i = 1
			j = nil
		end
		return
	else 	
        move.backward()
       	i = i + 1   
		return
	end
end

if a == "clear_floor" then
	if not j then
		say("I will clear the floor now")
		j = 1 
	end

	where = {"down","up","left_up" , "right_up", "left","right", "left_down","right_down"}
	if j < ( #where + 1 ) then 
        dir = where[j]
        while read_node[dir]() == "air" do
        	j = j + 1
			if j > #where then 
				j = ( #where + 1 )
				return
			else	dir = where[j]
			end
		end
        dig[dir]()
	    j = j + 1
	    return
	else
		say('Slice '.. i .. '/4 ...done.')
    	if i < 4 then
			n = read_node.forward()
			if n ~= "air" then dig.forward() ; return
			else 
				move.forward()
			    j = 1
				i = i + 1
				return
			end
		else
			if check_inventory.self(t) then place.right_down(t,1) end
			a = "turn_and_restart"
			i = 1
			j = nil
			say("Ok, let's get ready for the next step !")
			return
		end	
	end
end
	
if a == "turn_and_restart" then
	if i == 1 then move.down()
	elseif i == 2 then move.backward()
	elseif i == 3 then move.left()
	else 
		turn.left()
		 l = l + 1
    i = nil
		a = nil
		return
	end	
  i = i + 1
  return
end
