-- title : Mining_101
-- author : 
-- description : Simple digger 1x1
-- source : https://wiki.minetest.net/Mods/basic_robot
--

-- simple digger1x1
 if not i then  -- Init:
   i=0
   turn.left()  -- initital positioning: aim robot in a different direction
 --move.up()
 end
 
 -- Work:
 i=i+1  say(i)
 if i<=20 then
   dig.forward()
   move.forward()
 end
