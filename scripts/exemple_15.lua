-- title : Robot Penguin
-- author : ixfud
-- description :  Robot that look like a penguin and interact with the player. 
-- source : 
--

-- Initialise
if not i then 
   i = 0
   move.left()
   -- Change le texte au dessus du Robot
   self.label("Robot Penguin")
   -- Definit le modele 3D
   self.set_properties({
     visual = "mesh",
     mesh = "mobs_penguin.b3d",
     visual_size = {x = 0.35, y = 0.35},
     collisionbox = {-0.2, -0.0, -0.2,  0.2, 0.5, 0.2},
	textures = {"mobs_penguin.png"}
   })
elseif i < 6 then
  -- say(i.." - place")
 move.up()
 place.down("default:dirt")
else
  under = read_node.down()
  if (( under ~= "basic_robot:spawner" )
      and (under ~= "default:obsidian_block")) then
      
         dig.down()
         move.down()
  else
       i = 0
       turn.left()
       move.left()
  end
end

i = i + 1
