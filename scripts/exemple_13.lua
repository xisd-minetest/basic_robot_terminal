-- title : Ore-detector1
-- author : 
-- description : Find ores in a 9 nodes radius. 
-- source : https://wiki.minetest.net/Mods/basic_robot
--
-- TIP : Run this detector on the robot, and use an 'empty' remote controller to move the bot around to find the good stuff. 

 
 if (rg==nil) then s="Ore-detector v0.3"
   say(s)
   self.label(s)
   rg=9  i=1  next=0
   ore={'diamond', 'mese', 'gold', 'copper', 'iron', 'coal', '-'}  --Table of valueable stuff
 end
 
 if (ore[i]=="-") then next=1  i=0 end
 if next>0 then
   i=i+1  rg=9  next=0
   say("Seaching: "..ore[i])
 end
 if (rg>8) then rg=8 end
 
 f = find_nodes("default:stone_with_"..ore[i], rg)  -- returns true/false
 if f then
   say("Found "..ore[i].." within range "..rg) 
   rg=rg-1
 else
   --say("no "..i.." @ rg"..rg)
   next=1 
 end
