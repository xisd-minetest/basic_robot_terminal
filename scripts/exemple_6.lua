-- title : Run_1
-- author : 
-- description : The robot run and turn when hitting an obstacle
-- source : https://wiki.minetest.net/Mods/basic_robot
--

 if (i==nil) then say("Demo1"); i=0 end
 
 if read_node.forward()=="air" then
   move.forward()
 else
   turn.right()
 end

-- If nothing ("air" mean nothing solid) is in front of it, the robot moves forward, otherwise it turns.
-- The robot can "fly" one block above the ground, like the player can reach by jumping up. The bot just doesn't need to bounce up & down.

-- That means, if the robot reaches a place where the ground one step ahead is more then one block lower, he cannot move forward.
-- With the above program, he just stops moving, because there are no instructions in the code to handle that case. 
