-- title : Ore-detector2
-- author : 
-- description :  A faster, optimized version of the Ore-Detector1. 
-- source : https://wiki.minetest.net/Mods/basic_robot
--
-- TIP : Run this detector on the robot, and use an 'empty' remote controller to move the bot around to find the good stuff. 
-- It also searches for water and lava, to prevent surprises during mining. 
 
  if not s then s="Ore-detector2 v0.4f"
   say(s)
   self.label(s)
   rg=9  i=1  next=0  msg=""
 
   --Table of stuff to search:
   ore={'@diamond', '@mese', '@gold', 
  '@copper', '@iron', '@coal', 
 --'!torch', '!torch_wall',
  '!lava_source',  '!lava_flowing',
  '!water_source', '!water_flowing',
  '-'}   --todo: moreores, mobs
   say(#ore) --
 end
 
 if (ore[i]=="-") then next=1  i=0  
   --say("done.")  self.remove()
 end
 if next>0 then
   i=i+1  rg=9  next=0
   --msg="Seaching: "..ore[i]  say(msg)  msg=""
 end
 if (rg>8) then rg=8 end
 
 s  = ore[i]
 c1 = string.sub(s,1,1)
 s  = string.sub(s,2)
 if c1=="@" then s="default:stone_with_"..s end
 if c1=="!" then s="default:"           ..s end
 
 f = find_nodes(s, rg)  -- returns true/false
 if f then
   msg="Found "..s.." within range "..rg
   rg=rg-1
 else
 --say(i.." no "..s.." @ rg"..rg)  --debug
   if #msg>0 then say("--> "..msg)  end
   msg=""
   next=1 
 end
