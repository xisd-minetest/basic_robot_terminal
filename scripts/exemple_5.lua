-- title : Catch_Me_2
-- author : 
-- description : Now we go up, instead of down. :-)
-- source : https://wiki.minetest.net/Mods/basic_robot
--

 dig.up()
 move.up()
 local dirt_place = place.down("default:dirt")
 if not dirt_place then say("I need dirt in my inventory") end
 p = self.pos()  -- returns a table, with elements x, y, and z
 say("Position: " .. p.x .. "," ..  p.y .. "," ..  p.z)

-- Now we go up, instead of down.
-- After stopping the robot, and pressing start again,
-- there will be a block of dirt above the bot from the first go.
-- We need to dig away this block so that the bot can move up again.

-- The command "say()" outputs a single string, but several strings can be concatenated with '..'.
-- Numbers are automatically converted to strings for output (e.g. the number in p.x). 

-- Note: y is used as the height here in minetest. 
