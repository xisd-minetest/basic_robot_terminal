-- title : Catch_Me_1
-- author : 
-- description : Some program that works perfectly well, but still shoots the player's foot :-)
-- source : https://wiki.minetest.net/Mods/basic_robot
--

 dig.down()
 move.down()

-- The robot keeps digging down, and will continue unless it is stopped or it finds a cave, or an unloaded map-block.
-- With the first dig, it will also destroy its spawner.
-- So, to get at the stop-button, the user has to jump into the hole to get at the robot.
-- (Quickly, otherwise the player also gets falling damage !)

--  Leaving the game, and entering again probably also stops the bot... 
-- (Maybe a bug: without the spawner, the inventory of the robot is not accessible) 
