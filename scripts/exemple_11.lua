-- title : Big_Display
-- author : 
-- description : Show text from the book in slot#1 on a big screen. 
-- source : https://wiki.minetest.net/Mods/basic_robot
--

 
if not pn then pn="BigDisplay"  -- show news on big display
  title,text = book.read(1);
  if not text then  say("No text... \n Place a written book into the first slot of the library"); self.remove(); goto ex  end
 
  self.name("")
  self.label(title)
  self.display_text(text,20,3)  --triple-size, 20 char per line
 
--turn.left()  --turn into the direction of the player
  turn.right()
end
 
::ex::
