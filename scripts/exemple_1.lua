-- title : Hello_World_1
-- author : 
-- description : Say "Hello World", while spinning atop the spawner.
-- source : https://wiki.minetest.net/Mods/basic_robot
--

-- The robot will say "Hello World" again and again, while spinning atop the spawner.
-- To stop it, rightclick the robot or spawner, and press the Stop-button.

say("Hello World")
turn.left()


