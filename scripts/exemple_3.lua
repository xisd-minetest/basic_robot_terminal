-- title : Hello_World_3
-- author : 
-- description : Shorter version of Hello_World_2.
-- source : https://wiki.minetest.net/Mods/basic_robot
--

 if i==nil then i="Hi !"; say(i); i=0 end
 i=i+1  say(i)  turn.left()

-- Same as above, but with multiple statements in one line,
-- and some variations (e.g. braces and ";" are optional).

--    In lua, the value of a variables can be of any type. 
--    Here, i first is assigned the string "Hi !", and later a number. 
