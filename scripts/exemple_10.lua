-- title : Mining_103
-- author : rnd/hajo
-- description : A more advanced digger
-- source : https://wiki.minetest.net/Mods/basic_robot
--

-- rnd 2017-01-23 / hajo 2017-01-26
 if not commands then
      --turn.left()  -- aim robot in a different direction
      --move.up()
 
 script = "DuDdf"  -- Variant 1 - uses only dig-forward
 --	script = "^Df"    -- Variant 2: DigUp, DigForward, MoveForward
 
	commands = {
	["f"] = function() move.forward()  end,
	["b"] = function() move.backward() end,
	["l"] = function() move.left()  end,
	["r"] = function() move.right() end,
	["u"] = function() move.up()   end,
	["d"] = function() move.down() end,
 
	["<"] = function() turn.left()  end,
	[">"] = function() turn.right() end,
 
	["D"] = function() dig.forward() end,
	["^"] = function() dig.up() end,
	}
 
	i=1; n=string.len(script)
 end
 
 c=string.sub(script,i,i); commands[c]();
 if i>=n then i=1 else i=i+1 end
