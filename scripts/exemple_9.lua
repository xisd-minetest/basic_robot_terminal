-- title : Mining_102
-- author : 
-- description : simple digger 2x1
-- source : https://wiki.minetest.net/Mods/basic_robot
--

-- simple digger 2x1
 if not i then  -- Init:
   i=0
   turn.left()  -- initital positioning: aim robot in a different direction
 --move.up()
 end
 
 -- Work:
 i=i+1  say(i)
 if i<=20 then
   dig.forward()
   -- ??
   dig.up()       -- !! doesn't work with setting 'maxdig=1'
   move.forward()
 else
   say("stop")  self.remove()
 end
