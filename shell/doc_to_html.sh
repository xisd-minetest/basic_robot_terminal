#!/bin/sh
# Generate a html page from all the markdown documents present in rep 'docs'
# Require 'markdown' command
#

#-- Markdown docs path
docpath="../docs"
if [ ! -d "$docpath" ];then exit ;fi

#-- Temporary files
html_menu_tmp="~html_doc_menu.tmp"
html_content_tmp="~html_doc_content.tmp"
if [ -f "$html_menu_tmp" ];then 
	echo "The temporary destination file '$html_menu_tmp' already exist. Aborting"
	exit
fi
if [ -f "$html_content_tmp" ];then 
	echo "The temporary destination file '$html_menu_tmp' already exist. Aborting"
	exit
fi
#-- output file
output="../robot_doc.html"
# if [ -f "$output" ];then echo "The output file '${output}' already exist"; exit; fi

#-- Initialize files
echo "<h1>Robot Documentation</h1>" > "$html_menu_tmp"
echo "" > "$html_content_tmp"
echo "<a name='top'></a><ul>" >> "$html_menu_tmp"


#-- Loop through file
files=$(ls $docpath |grep -e "^robot_doc_.*\.md$")
for mddoc in $files ; do
	#-- Full path
	doc="${docpath}/${mddoc}" 

	#-- Get infos
	pat_title="\-\- title :"
	title=$(grep "$pat_title" $doc |sed "s#$pat_title##")
	pat_description="\-\- description :"
	description=$(grep "$pat_description" $doc |sed "s#$pat_description##")

	#-- Menu entry
	echo "	<li><a href=\"#${mddoc}\">${title}</a>" >> "$html_menu_tmp"
	echo "	<br> - ${description}</li>" >> "$html_menu_tmp"

	#-- Anchor
	echo "<hr>" >> "$html_content_tmp"
	echo "<a name=\"${mddoc}\"></a><a href=\"#top\">-- Return to Index --</a><br>" >> "$html_content_tmp"

	#-- Content
	markdown "$doc" >> "$html_content_tmp"
	echo "" >> "$html_content_tmp"
done
echo "</ul>" >> "$html_menu_tmp"

#-- Join files
echo "" > "$output"
cat "$html_menu_tmp" >> "$output"
cat "$html_content_tmp" >> "$output"

#-- Delete temporary files
rm -f "$html_menu_tmp"
rm -f "$html_content_tmp"
